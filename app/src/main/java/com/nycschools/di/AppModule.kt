package com.nycschools.di

import com.nycschools.data.remote.NYCSchoolAPI
import com.nycschools.repository.NYCSchoolRepository
import com.nycschools.util.Constants
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object AppModule {

    // this is both way binding happening from repository to api(service) class
    @Singleton
    @Provides
    fun provideNYCRepository(
        api: NYCSchoolAPI
    ) = NYCSchoolRepository(api)

    // returning retrofit call
    @Singleton
    @Provides
    fun provideNYCSchoolApi(): NYCSchoolAPI {
        return Retrofit.Builder()
            .baseUrl(Constants.BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .create(NYCSchoolAPI::class.java)
    }
}