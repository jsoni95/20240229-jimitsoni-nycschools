package com.nycschools.repository

import com.nycschools.data.remote.NYCSchoolAPI
import com.nycschools.data.remote.responses.NYCSATResultList
import com.nycschools.data.remote.responses.NYCSchoolsList
import com.nycschools.util.Resource
import dagger.hilt.android.scopes.ActivityScoped
import javax.inject.Inject

/*
    Repository is connected with api (service) class and implemented suspend functions
 */

@ActivityScoped
class NYCSchoolRepository @Inject constructor(
    private val api: NYCSchoolAPI
) {
    suspend fun getNYCSchoolsList(): Resource<NYCSchoolsList> {
        val response = try {
            api.getNYCSchoolsList()
        }catch (e: Exception) {
            return Resource.Error("An Unknown error Happened. Please try Again!")
        }
        return Resource.Success(response)
    }

    suspend fun getSatScores(dbn: String): Resource<NYCSATResultList> {
        val response = try {
            api.getSatScores(dbn)
        }catch (e: Exception) {
            return Resource.Error("An unknown Error Happen. Please try again!")
        }
        return Resource.Success(response)
    }
}