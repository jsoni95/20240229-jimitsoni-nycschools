package com.nycschools.presentation.nycSchoolSATScore

import androidx.lifecycle.ViewModel
import com.nycschools.data.remote.responses.NYCSATResultList
import com.nycschools.repository.NYCSchoolRepository
import com.nycschools.util.Resource
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class NYCSchoolSATScoreViewModel @Inject constructor(
    private val repository: NYCSchoolRepository
): ViewModel() {

    suspend fun getSelectedSchoolSATSCoreOverview(dbn: String): Resource<NYCSATResultList> {
        return repository.getSatScores(dbn)
    }

}