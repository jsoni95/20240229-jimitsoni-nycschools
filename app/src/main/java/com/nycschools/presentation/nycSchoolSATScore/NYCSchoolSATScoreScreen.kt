package com.nycschools.presentation.nycSchoolSATScore

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.offset
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.ElevatedCard
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.produceState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.draw.shadow
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import com.nycschools.R
import com.nycschools.data.remote.responses.NYCSATResultList
import com.nycschools.data.remote.responses.NYCSATResultListItem
import com.nycschools.ui.theme.Roboto
import com.nycschools.ui.theme.RobotoCondensed
import com.nycschools.util.Resource

@Composable
fun NYCSchoolSATScoreScreen(
    viewModel: NYCSchoolSATScoreViewModel = hiltViewModel(),
    dbn: String,
    navController: NavController
) {
    /*
        This code is using produceState function which is part of jetpack compose framework
        which is used to produce state based on a given coroutine block and update the UI when the state changes
     */
    val satScoreInformation =
        produceState<Resource<NYCSATResultList>>(initialValue = Resource.Loading()) {
            value = viewModel.getSelectedSchoolSATSCoreOverview(dbn)
        }.value

    Box(
        modifier = Modifier
            .fillMaxSize()
            .background(MaterialTheme.colorScheme.background)
            .padding(bottom = 16.dp)
    ) {
        SATScoreTopSection(
            navController = navController,
            modifier = Modifier
                .fillMaxWidth()
                .fillMaxHeight(0.2f)
                .align(Alignment.TopCenter)
        )
        Spacer(modifier = Modifier.padding(bottom = 80.dp))
        Column(
            modifier = Modifier
                .fillMaxWidth()
                .padding(16.dp),
            horizontalAlignment = Alignment.CenterHorizontally,
            verticalArrangement = Arrangement.SpaceBetween
        ) {
            Spacer(modifier = Modifier.padding(top = 40.dp))
            SATStateWrapper(
                satScoreInfo = satScoreInformation,
                modifier = Modifier
                    .fillMaxSize()
                    .padding(
                        top = 20.dp,
                        start = 16.dp,
                        end = 16.dp,
                        bottom = 16.dp
                    )
                    .shadow(10.dp, RoundedCornerShape(10.dp))
                    .clip(RoundedCornerShape(10.dp))
                    .background(MaterialTheme.colorScheme.surface)
                    .padding(16.dp)
                    .align(Alignment.CenterHorizontally),
                loadingModifier = Modifier
                    .size(100.dp)
                    .align(Alignment.CenterHorizontally)
                    .padding(
                        top = 20.dp,
                        start = 16.dp,
                        end = 16.dp,
                        bottom = 16.dp
                    )
            )
        }
    }
}


@Composable
fun SATScoreTopSection(
    navController: NavController,
    modifier: Modifier = Modifier
) {
    Box(
        contentAlignment = Alignment.TopStart,
        modifier = modifier
            .background(
                Brush.verticalGradient(
                    listOf(
                        Color.Black,
                        Color.Transparent
                    )
                )
            )
    ) {
        Icon(
            imageVector = Icons.Default.ArrowBack,
            contentDescription = null,
            tint = Color.White,
            modifier = Modifier
                .size(36.dp)
                .offset(16.dp, 16.dp)
                .clickable {
                    navController.popBackStack()
                }
        )
    }
}

@Composable
fun SATStateWrapper(
    satScoreInfo: Resource<NYCSATResultList>,
    modifier: Modifier = Modifier,
    loadingModifier: Modifier = Modifier
) {
    when (satScoreInfo) {
        is Resource.Success -> {
            if (satScoreInfo.data?.isNotEmpty() == true) {
                satScoreInfo.data.forEach { satScore ->
                    SelectedSchoolSATDetail(
                        satScoreInfo = satScore,
                        modifier = Modifier.padding(10.dp)
                    )
                }
            } else {
                NoSATScoreForSchool()
            }
        }

        is Resource.Error -> {
            Text(
                text = satScoreInfo.message!!,
                color = Color.Red,
                modifier = modifier
            )
        }

        is Resource.Loading -> {
            CircularProgressIndicator(
                color = MaterialTheme.colorScheme.primary,
                modifier = loadingModifier
            )
        }
    }
}

@Composable
fun SelectedSchoolSATDetail(
    satScoreInfo: NYCSATResultListItem,
    modifier: Modifier = Modifier
) {
    ElevatedCard(
        elevation = CardDefaults.cardElevation(
            defaultElevation = 6.dp
        ),
        modifier = Modifier
            .fillMaxWidth()
            .padding(8.dp)
    ) {
        Column(
            modifier = Modifier
                .fillMaxWidth()
                .padding(16.dp),
            horizontalAlignment = Alignment.CenterHorizontally,
            verticalArrangement = Arrangement.SpaceBetween
        ) {
            Text(
                text = satScoreInfo.schoolName,
                fontWeight = FontWeight.Bold,
                fontFamily = Roboto,
                fontSize = 16.sp,
            )
            Text(
                text = "Total Students Took SAT: ${satScoreInfo.satTotalStudentsTaker}",
                fontFamily = RobotoCondensed,
                fontSize = 14.sp
            )

            Text(
                text = "Average Math Score: ${satScoreInfo.satMathAverageScore}",
                fontFamily = RobotoCondensed,
                fontSize = 14.sp
            )

            Text(
                text = "Average Reading Score: ${satScoreInfo.satReadingAverageScore}",
                fontFamily = RobotoCondensed,
                fontSize = 14.sp
            )

            Text(
                text = "Average Writing Score: ${satScoreInfo.satWritingAverageScore}",
                fontFamily = RobotoCondensed,
                fontSize = 14.sp
            )

        }
    }
}

@Composable
fun NoSATScoreForSchool() {
    ElevatedCard(
        elevation = CardDefaults.cardElevation(
            defaultElevation = 6.dp
        ),
        modifier = Modifier
            .fillMaxWidth()
            .padding(8.dp)
    ) {
        Column(
            modifier = Modifier
                .fillMaxWidth()
                .padding(16.dp),
            horizontalAlignment = Alignment.CenterHorizontally,
            verticalArrangement = Arrangement.SpaceBetween
        ) {
            Text(
                text = stringResource(R.string.sat_scores_not_available_for_this_school),
                fontWeight = FontWeight.Bold,
                fontFamily = Roboto,
                fontSize = 16.sp,
            )
        }
    }
}