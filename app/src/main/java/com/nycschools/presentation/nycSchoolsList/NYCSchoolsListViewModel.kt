package com.nycschools.presentation.nycSchoolsList

import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.nycschools.data.model.SchoolList
import com.nycschools.repository.NYCSchoolRepository
import com.nycschools.util.Resource
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class NYCSchoolsListViewModel @Inject constructor(
    private val repository: NYCSchoolRepository
): ViewModel() {
    var nycSchoolsList = mutableStateOf<List<SchoolList>>(listOf())
    var loadError = mutableStateOf("")
    var isLoading = mutableStateOf(false)

    private var cachedNYCSchoolsList = listOf<SchoolList>()
    private var isSearchStarting = true
    var isSearching = mutableStateOf(false)

    init {
        loadNYCSchoolsList()
    }

    // this function is to load school list using coroutines and also use of Resource class to display states while making api call
    private fun loadNYCSchoolsList() {
        viewModelScope.launch {
            isLoading.value = true
            val result = repository.getNYCSchoolsList()
            when(result) {
                is Resource.Success -> {
                    val listOfNYCSchool = result.data?.map{ entry ->
                        SchoolList(
                            entry.dbn,
                            entry.school_name,
                            entry.address,
                            entry.city,
                            entry.state,
                            entry.zip,
                            entry.totalStudents,
                            entry.grades
                        )
                    }
                    loadError.value = ""
                    isLoading.value = false
                    if (listOfNYCSchool != null) {
                        nycSchoolsList.value = listOfNYCSchool
                    }
                }
                is Resource.Error -> {
                    loadError.value = result.message ?: ""
                    isLoading.value = false
                }
                else -> {
                    isLoading.value = false
                }
            }
        }
    }

    //this function is to search via school name or DBN number
    fun searchNYCSchoolsList(query: String) {
        val listToSearch = if(isSearchStarting) nycSchoolsList.value else cachedNYCSchoolsList
        viewModelScope.launch(Dispatchers.Default) {
            if(query.isEmpty()) {
                nycSchoolsList.value = cachedNYCSchoolsList
                isSearching.value = false
                isSearchStarting = true
                return@launch
            }

            val results = listToSearch.filter {
                it.schoolName.contains(query.trim(), ignoreCase = true) || it.dbn.toString() == query.trim()
            }

            if(isSearchStarting) {
                cachedNYCSchoolsList = nycSchoolsList.value
                isSearchStarting = false
            }

            nycSchoolsList.value = results
            isSearching.value = true
        }
    }

    // this function to click on retry button when there is an error with network call
    fun retryToLoadNYCSchoolList() {
        loadNYCSchoolsList()
    }
}