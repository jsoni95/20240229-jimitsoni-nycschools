package com.nycschools.util

/*
    this class is created to make use of Success, Error or Loading states while making api call
    when it's loading then we show circularProgressIndicator
    when it's error we can display error message in red color
    when it's success then we display data
 */

sealed class Resource <T> (val data: T? = null, val message: String?  = null) {
    class Success<T>(data: T?): Resource<T>(data)
    class Error<T>(message: String?, data: T? = null): Resource<T>(data, message)
    class Loading<T>(data: T? = null): Resource<T>(data)
}