package com.nycschools

import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.navigation.NavController
import androidx.navigation.NavHostController
import androidx.navigation.NavType
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.navArgument
import com.nycschools.presentation.nycSchoolSATScore.NYCSchoolSATScoreScreen
import com.nycschools.presentation.nycSchoolsList.NYCSchoolListScreen

class NavigationSetup(private val navController: NavController) {

    @Composable
    fun NavigationContent() {
        NavHost(
            navController = navController as NavHostController,
            startDestination = "nyc_schools_list_screen"
        ) {
            composable("nyc_schools_list_screen") {
                NYCSchoolListScreen(navController = navController)
            }
            composable(
                "nyc_school_sat_score_screen/{dbn}",
                arguments = listOf(
                    navArgument("dbn") {type = NavType.StringType}
                )
            ) {
                val dbn = remember {
                    it.arguments?.getString("dbn")
                }
                NYCSchoolSATScoreScreen(
                    dbn = dbn ?: "",
                    navController = navController
                )
            }
        }
    }
}