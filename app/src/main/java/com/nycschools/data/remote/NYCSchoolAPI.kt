package com.nycschools.data.remote

import com.nycschools.data.remote.responses.NYCSATResultList
import com.nycschools.data.remote.responses.NYCSchoolsList
import retrofit2.http.GET
import retrofit2.http.Query

/*
 this is a service class which is connected to repository
 */

interface NYCSchoolAPI {
    @GET("s3k6-pzi2.json")
    suspend fun getNYCSchoolsList(): NYCSchoolsList

    @GET("f9bf-2cp4.json")
    suspend fun getSatScores(
        @Query("dbn") dbn: String
    ): NYCSATResultList
}