package com.nycschools.data.remote.responses

import com.google.gson.annotations.SerializedName

/*
	{
		"dbn": "17K548",
		"school_name": "BROOKLYN SCHOOL FOR MUSIC & THEATRE",
		"num_of_sat_test_takers": "48",
		"sat_critical_reading_avg_score": "385",
		"sat_math_avg_score": "393",
		"sat_writing_avg_score": "373"
	}
 */

data class NYCSATResultListItem(
    val dbn: String,
    @SerializedName("num_of_sat_test_takers")
    val satTotalStudentsTaker: String,
    @SerializedName("sat_critical_reading_avg_score")
    val satReadingAverageScore: String,
    @SerializedName("sat_math_avg_score")
    val satMathAverageScore: String,
    @SerializedName("sat_writing_avg_score")
    val satWritingAverageScore: String,
    @SerializedName("school_name")
    val schoolName: String
)