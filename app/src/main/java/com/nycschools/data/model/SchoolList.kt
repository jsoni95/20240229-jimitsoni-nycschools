package com.nycschools.data.model

/*
  This is the only data we require to display on NYCSchoolListScreen
 */

data class SchoolList(
    val dbn: String,
    val schoolName: String,
    val address: String,
    val city: String,
    val state: String,
    val zip: String,
    val total: String,
    val grades: String
)
