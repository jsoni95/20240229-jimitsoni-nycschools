# 20240229-JimitSoni-NYCSchools

## Project Information

- This project is created using MVVM Architecture, Jetpack Compose, Coroutines, Retrofit, GSON, Dagger Hilt

## MVVM Overview

## data directory
- model -> schoolList -> this model created to just map the data that we require to display on NYCSchoolListScreen
- remote -> responses -> this is the data classes created using JSON response that we are getting using Postman or Insomnia
- remote -> NYCSchoolAPI -> this is service class to make GET Calls to retrieve data in suspend functions

## di directory
- AppModule -> It is used for binding repository to api class by using dagger hilt for dependency injection and making retrofit call

## presentation directory
- nycSchoolsList -> this directory contains viewmodel and screen. In this screen, when user clicks on any school it will navigate to next screen using it's dbn number
- nycSchoolSATScore - this directory contains viewmodel and screen. In this screen, display school's sat score overview and user can also navigate back to nycSchoolsList

## repository directory
- nycSchoolRepository -> it is a class which purpose to provide a clean api for accessing data

## util directory
- constants - it is useful to put const strings or urls
- Resource - it is sealed class to create a states for Success, Error, Loading while making api call to display different UI for all the states

## NavigationSetUp
- this class is set up for navigating between two screens

## MainActivity 
- this is a main entry point for application to run

## NYCSchoolsApplication
- Defining application name


## Screenshots of UI 

## NYCSchoolsListScreen

![nycSchoolsList.png](nycSchoolsList.png)

## NYCSchoolSATScoreScreen
![img.png](nycSchoolSATScoreScreen.png)
